# PIPA-frontend

O PIPA é uma plataforma com foco em centralizar o gerenciamento de autorização de diversos serviços de autorização.


### Configuração do Ambiente
É obrigatório ter as seguintes dependências instaladas para o build do projeto:

- Node.js

Instalar dependências, em /frontend, digitar o comando:
```cli
npm install
```

Executar o programa
```cli
/frontend: npm start
```


### Tecnologias

- React.js



